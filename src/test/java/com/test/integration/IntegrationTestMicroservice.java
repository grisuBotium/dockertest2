package com.test.integration;


import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertFalse;

import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import com.palantir.docker.compose.DockerComposeRule;
import com.palantir.docker.compose.configuration.ProjectName;
import com.palantir.docker.compose.connection.waiting.HealthChecks;
import com.test.category.IntegrationTest;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

@Category(IntegrationTest.class)
public class IntegrationTestMicroservice {

    private static final String WIREMOCK_SERVICE = "wiremock";
    private static final String WIREMOCK_PORT = "9090";

    // start wiremock service before test execution and tear down after
    @ClassRule
    public static DockerComposeRule docker = DockerComposeRule.builder()
            .file("src/test/resources/docker-compose-wiremock.yml")
            .projectName(ProjectName.random())
            .waitingForService(WIREMOCK_SERVICE, HealthChecks.toHaveAllPortsOpen())
            .build();
    
    // rest assured initialization
    @BeforeClass
    public static void setup() {
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(WIREMOCK_PORT);
        }
        else {
            RestAssured.port = Integer.valueOf(port);
        }
        String basePath = System.getProperty("server.base");
        if(basePath == null) {
            basePath = "/";
        }
        RestAssured.basePath = basePath;
        String baseHost = System.getProperty("server.host");
        if(baseHost == null) {
            baseHost = "http://localhost";
        }
        RestAssured.baseURI = baseHost;
        System.out.println("******** Port: " + RestAssured.port 
        		         + " ******** BasePath: " + RestAssured.basePath
        		         + " ******** BaseHost: " + RestAssured.baseURI + " ********");
    }

    // dummy test
    @Test
    public void P0_DummyTest() throws Exception {
        assertFalse("Test", false);
    }
    
    // testing accounts service with rest assured
    @Test
    public void P1_MultipleAccounts() {   	    	
    	given()
    		.header("Content-Type", "application/json\r\n")
    		.header("API-key", "00000000-1212-0f0f-a0a0-123456789abc")
    		.header("Authorization", "Bearer AbCdEf123456")
    	.when().get("/accounts").then()
        	.statusCode(200)
        	.contentType(ContentType.JSON)
        	.body("accounts.accountId", Matchers.hasItems("D2C8C1DCC51A3738538A40A4863CA288E0225E52", 
        												  "D2C8C1DCC51A3738538A40A4863CA288E0225E53"));
    }
}
